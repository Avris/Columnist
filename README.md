# Columnist – Simple and lightweight multi-column design

Columnist achieves a nice, masonry-like, multi-column design without using absolute positioning.
It doesn't move items to different columns, doesn't do any fancy maths,
all it does it fixing the empty space below shorter columns – simple and lightweight!

 - [Demo](https://columnist.avris.it)
 - [How it works?](https://avris.it/projects/columnist)
 - [Source code](https://gitlab.com/Avris/Columnist) 
 - [NPM](https://www.npmjs.com/package/avris-columnist) 

## Installation

You can either install Columnist as a node module:

    $ npm i --save avris-columnist
    
    or
    
    $ yarn add avris-columnist

Use a CDN:

    <script type="module">
        import Columnist from 'https://cdn.jsdelivr.net/npm/avris-columnist@0.3.4/+esm';
    </script>

## Usage

### HTML

    <div class="columnist-wall row">
        <div class="columnist-column col-4">
            <div class="card">...</div>
        </div>
        ...
    </div>

Class names are irrelevant. It's important that a column has only one child element!

### JS

    import Columnist from 'avris-columnist';
    
    const columnist = new Columnist(document.querySelector('.columnist-wall'));
    
    columnist.layout();  // apply layout 
    
    columnist.start(100);  // run every 100 ms -- temporary solution, will be removed
    
    columnist.destroy();  // destroy and clean up
    
### CSS

You can add an animation like this:

    <style>
        .columnist-wall > .columnist-column {
            transition: margin-top .2s ease-in-out;
        }
    </style>


## Copyright
 
 * **Author:** Andrea Vos [(avris.it)](https://avris.it)
 * **Licence:** [OQL](https://oql.avris.it/license.tldr?c=Andrea%20Vos%7Chttps://avris.it)
